lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
env = ENV['MAVIA_ENV'] ? ENV['MAVIA_ENV'].to_sym : :development
require 'rubygems'
require 'bundler'

Bundler.require(:default, env)

require 'mavia'
require_relative 'data_manager/ext/core_ext'
require_relative 'data_manager/config/config'
require_relative 'data_manager/mixins/mixins'
require_relative 'data_manager/service_utils/service_utils'
require_relative 'data_manager/errors'
require_relative 'data_manager/logger'
require_relative 'data_manager/databases/databases'

module DataManager

  def self.run(run_opts = {})
    process = run_opts[:process]

    case process
    when :xirgo
      run_xirgo
    else
      fail Mavia::Errors::UnsupportedProcessType.new(process)
    end

    #binding.pry
    DataManager.log 'Workers are working. Supervisor is supervising. Time to take a nap.'
    sleep
  end

  private

  def self.run_xirgo
    require_relative 'data_manager/models/xirgo/models'
    require_relative 'data_manager/messages/xirgo/messages'

    callback = Proc.new do |body|
      DataManager.log "Message received: #{body}"

      message_objects = DataManager::Messages::Xirgo::Base.message_factory(body)
      message_objects.each { |message_object| message_object.execute }
    end

    supervisor = Mavia::Workers::IncomingMessageWorkerSupervisor.new(DataManager.opts[:xirgo][:rabbit_url], DataManager.opts[:xirgo][:rabbit_queue])
    supervisor.run(callback)
    #binding.pry
  end

end
