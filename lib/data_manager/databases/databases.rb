require 'uri'

module DataManager
  module Database
    #DB = Mavia::Database.connect(DataManager.opts[:db_url])
    DB = Mavia::Database.connect(DataManager.opts[:db_url_no_auth], user: DataManager.opts[:db_user], password: DataManager.opts[:db_password])
  end
end