#TODO: Refactor all trip_summary stuff into TripSummary
module DataManager
  module Models
    class Device < Mavia::Models::Device
      include_associations(self)

      # This basically fills the role of Mavia Message 1.2 VehicleInformation, in that it's only real role is to assign/re-assign a Car to a Device
      # It is called whenever a "car-related" message is received for a Device that has no Car associated, or when a heartbeat message is received (4006 or 6015) (see Messages::Xirgo::Mixins::UpdatesCar).
      def associate_with_car(fields, raw_message)
        lock!
        success = false
        vin = fields[:vin]

        if vin.blank?
          fail DataManager::Errors::MissingMessageData.new(:vin, fields)
        elsif self.user_account.blank?
          fail DataManager::Errors::DeviceNotAttachedToUser.new('Device#associate_with_car', device_id)
        else

          # if there's already a car associated with this device, and it has the same VIN as the car the device indicates it's attached to, then don't bother going any further because it's already correctly associated and doing more is just a waste of time and resources
          unless self.car && self.car.VIN == vin
            fields_copy = fields.clone
            fields_copy[:device_event_id] = DataManager.opts[:device_event_ids][:vehicle_switched]
            #car = self.user_account.cars_dataset.where('Cars.VIN = :vin AND UserCars.WasDeleted = false AND UserCars.UserCarPermissionsId = :owner_permission', vin: vin, owner_permission: DataManager.opts[:user_car_permissions][:owner]).first
            car = self.user_account.cars_dataset.where('Cars.VIN = :vin AND UserCars.WasDeleted = false', vin: vin).first

            if car.blank?
              #remove current self.car, because it is no longer the correct car
              self.car = nil
              fields_copy[:description] = DataManager::ServiceUtils::Descriptions.vehicle_info_vehicle_not_moved(vin)
              #TODO: This method should call something that does what CarService.FireCarDeviceAssociationNotification does
              success = true
            else
              self.car = car
              fields_copy[:description] = DataManager::ServiceUtils::Descriptions.vehicle_info_vehicle_moved
              fields_copy[:car_event_id] = DataManager.opts[:car_event_ids][:misc_info]
              success = true if self.car.persist_event_history(fields_copy)
            end

            b64_raw_message = DataManager::ServiceUtils::Encoders::Base64.encode(raw_message)
            success = save && persist_event_history(fields_copy, b64_raw_message) && success
            fail DataManager::Errors::UnableToAssociateDeviceWithCar.new('xirgo/Device#associate_with_car', self.Id) unless success
          end
        end
      end

      def persist_event_history(fields, raw_message)
        rep_time = fields[:time]
        desc = fields[:description]
        event_id = fields[:device_event_id]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif desc.blank?
          fail DataManager::Errors::MissingMessageData.new(:description, fields)
        elsif event_id.blank?
          fail DataManager::Errors::MissingMessageData.new(:device_event_id, fields)
        else
          desc = desc.slice(0, 512)
          deh = Models::DeviceEventHistory.new(DeviceEventTypeId: event_id, MessagePayload: raw_message, Description: desc, DateReported: rep_time)
          deh.device = self
          deh.car = self.car unless self.car.blank?
          deh.save
        end
      end

      def start_trip_summary(fields)
        if has_no_car?
          DataManager.log DataManager::Errors::DeviceNotAttachedToCar.new('DataManager::Models::Device#start_trip_summary', fields)
        else
          lock!
          self.car.lock!

          rep_time = fields[:time]
          lat = fields[:lat]
          lng = fields[:lng]
          num_sat = fields[:num_sat]

          if rep_time.blank?
            fail DataManager::Errors::MissingMessageData.new(:time, fields)
          else
            ts = find_latest_or_create_trip_summary_by_time(rep_time)
            ts.lock!

            #if the message doesn't have a valid lat, lng, or num_sat, default them to last known values, or 0 if no last known values
            lat, lng = self.car.LastKnownGPSCoords ? self.car.LastKnownGPSCoords.split(',') : [0.0, 0.0] if lat.blank? || lng.blank?
            num_sat = self.car.LastNumberOfSatellites ? self.car.LastNumberOfSatellites : 0 if num_sat.blank?
            update_fields = {}
            update_fields[:StartPosLat] = lat
            update_fields[:StartPosLong] = lng
            update_fields[:StartPosAccuracy] = num_sat
            update_fields[:TripStartTime] = rep_time
            update_fields[:StartMileageCounter] = fields[:miles] ? fields[:miles] : 0.0

            ts.set_fields(update_fields, update_fields.keys)

            update_current_trip_summary_if_trip_summary_newer(ts)
            ts.save_changes
          end
        end
      end

      def finish_trip_summary(fields)
          if has_no_car?
            DataManager.log.info DataManager::Errors::DeviceNotAttachedToCar.new('DataManager::Models::Device#finish_trip_summary', fields)
          else
          lock!
          self.car.lock!

          rep_time = fields[:time]
          lat = fields[:lat]
          lng = fields[:lng]
          num_sat = fields[:num_sat]
          voltage = fields[:voltage] ? fields[:voltage] : 0.0
          fuel_level = fields[:fuel_level] ? fields[:fuel_level] : 0.0

          if rep_time.blank?
            fail DataManager::Errors::MissingMessageData.new(:time, fields)
          else
            ts = find_latest_or_create_trip_summary_by_time(rep_time)
            ts.lock!

            #if the message doesn't have a valid lat, lng, or num_sat, default them to last known values, or 0 if no last known values
            lat, lng = self.car.LastKnownGPSCoords ? self.car.LastKnownGPSCoords.split(',') : [0.0, 0.0] if lat.blank? || lng.blank?
            num_sat = self.car.LastNumberOfSatellites ? self.car.LastNumberOfSatellites : 0 if num_sat.blank?
            update_fields = {}
            update_fields[:EndPosLat] = lat
            update_fields[:EndPosLong] = lng
            update_fields[:EndPosAccuracy] = num_sat
            update_fields[:TripEndTime] = rep_time
            update_fields[:TripTimeSecs] = (rep_time - ts.TripStartTime).round #TODO: this could arrive before a trip has a real start time (= unix epoch) and thus have a very large trip time
            update_fields[:TripCompleted] = true
            update_fields[:EstFuelUsed] = ts.TripMiles / ts.TripMPG if ts.TripMiles && ts.TripMPG #TODO: janky
            update_fields[:EndFuelLevel] = fuel_level
            update_fields[:EndVoltage] = voltage

            ts.set_fields(update_fields, update_fields.keys)

            ts.save_changes
          end
        end
      end

      def has_car?
        lock!

        !self.car.blank?
      end

      def has_no_car?
        !has_car?
      end

      private

      def find_latest_or_create_trip_summary_by_time(rep_time)

        correct_ts = find_latest_trip_summary_by_time(rep_time)

        # if there is no trip_summary that we can determine definitely matches for this trip_id and time combination, then create a new one, and return it,
        # otherwise, we have found a matching trip summary, so return that one
        if correct_ts.blank?
          correct_ts = TripSummary.new(TripStartTime: DataManager::ServiceUtils::DateTime.unix_epoch, TripEndTime: DataManager::ServiceUtils::DateTime.end_of_days)
          correct_ts.device = self
          correct_ts.car = self.car
          correct_ts.save
        end

        correct_ts
      end

      def find_latest_trip_summary_by_time(rep_time)
        valid_tss = trip_summaries_with_valid_bound_times(rep_time)

        # if we have trip_summaries within which the rep_time falls, and they have real start and end times, pick the one that started latest
        correct_ts = trip_summary_with_last_completed_start_and_end(valid_tss)

        # if we have trip_summaries within which the rep_time falls, and they don't have both real start and end times, find the ones
        # that have a real start time, and pick the one that started latest
        if correct_ts.blank?
          correct_ts = trip_summary_with_last_completed_start(valid_tss)
        end

        # if we have trip_summaries within which the rep_time falls, and they don't have both real start and end times, and none have real start times,
        # find the ones that have a real end time, and pick the one that ended latest
        if correct_ts.blank?
          DataManager.log "Device#find_latest_trip_summary_by_time entered an unexpected state. Valid trip_summaries: #{valid_tss.all}"
          correct_ts = trip_summary_with_last_completed_end(valid_tss)
        end

        # if we have trip_summaries within which the rep_time falls, and they don't have both real start and end times, and none have real start times,
        # and none have real end times, pick the first one, because we have no way of knowing which one is correct
        # (hopefully we never have more than 1 trip summary that fits into this bucket; if we do, something probably went really wrong and we need to dig deeper)
        if correct_ts.blank?
          valid_tss.first
        end

        correct_ts
      end

      # we need to subtract some number of seconds from rep_time here to make sure we catch any messages with a rep_time slightly after the trip's 'end time'
      def trip_summaries_with_valid_bound_times(rep_time)
        self.trip_summaries_dataset.for_update.where('TripStartTime <= :start_time AND TripEndTime >= :end_time', start_time: rep_time, end_time: DataManager::ServiceUtils::DateTime.end_time_with_setback(rep_time)).order(Sequel.desc('TripStartTime'))
      end

      # this is already sorted on TripStartTime DESC from the select in trip_summaries_with_valid_bound_times, so no need to do a secondary sort of the array
      def trip_summary_with_last_completed_start_and_end(trip_summaries)
        trip_summaries.where('TripStartTime != :epoch_start AND TripEndTime != :end_of_days', {epoch_start: DataManager::ServiceUtils::DateTime.unix_epoch, end_of_days: DataManager::ServiceUtils::DateTime.end_of_days}).first
      end

      # this is already sorted on TripStartTime DESC from the select in trip_summaries_with_valid_bound_times, so no need to do a secondary sort of the array
      def trip_summary_with_last_completed_start(trip_summaries)
        trip_summaries.where('TripStartTime != :epoch_start', epoch_start: DataManager::ServiceUtils::DateTime.unix_epoch).first
      end

      # here we have to re-sort the array on TripEndTime DESC to make sure we get the most recent trip_summary
      def trip_summary_with_last_completed_end(trip_summaries)
        trip_summaries.where('TripEndTime != :end_of_days', end_of_days: DataManager::ServiceUtils::DateTime.end_of_days).order(Sequel.desc('TripEndTime')).first
      end

      def update_current_trip_summary_if_trip_summary_newer(trip_summary)
        cts = self.car.current_trip_summary

        if cts.blank? || cts.TripStartTime < trip_summary.TripStartTime
          self.car.current_trip_summary = trip_summary
          self.car.save_changes
        end
      end

      #TODO: These likely will not be used for Xirgo, so remove when done refactoring
      #def average_and_max_speeds_in_kph(speeds)
      #  speeds = speeds.reject { |s| s[1] == 0 }
      #  speed_count = speeds.count
      #  if speed_count == 0
      #    [0.0, 0.0]
      #  else
      #    speed_total = speeds.reduce(0.0) { |sum, s| sum + s[1].to_f }
      #    avg_speed = speed_total / speed_count
      #    max_speed = speeds.max { |a, b| a[1] <=> b[1] }
      #    [avg_speed, max_speed[1]]
      #  end
      #end
      #
      #def average_and_max_speeds_in_mph(speeds)
      #  kph_speeds = average_and_max_speeds_in_kph(speeds)
      #  kph_speeds.map! { |s| DataManager::ServiceUtils::Converters.kph_to_mph(s) }
      #end
      #
      #def sum_idling(idling)
      #  if idling.count == 0
      #    0
      #  else
      #    idling.reduce(0.0) { |sum, i| sum + i[1].to_f }
      #  end
      #end

    end
  end
end