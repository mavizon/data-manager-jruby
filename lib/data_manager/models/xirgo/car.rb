#TODO: Refactor any TripSummary stuff into TripSummary
module DataManager
  module Models
    class Car < Mavia::Models::Car
      include_associations(self)

      STATES = {DataManager::ServiceUtils::Descriptions.vehicle_event_ignition => 1, DataManager::ServiceUtils::Descriptions.vehicle_event_driving => 2, DataManager::ServiceUtils::Descriptions.vehicle_event_idle => 3, DataManager::ServiceUtils::Descriptions.vehicle_event_park => 4}.freeze

      def persist_event_history(fields)
        rep_time = fields[:time]
        desc = fields[:description]
        event_id = fields[:car_event_id]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif desc.blank?
          fail DataManager::Errors::MissingMessageData.new(:description, fields)
        elsif event_id.blank?
          fail DataManager::Errors::MissingMessageData.new(:car_event_id, fields)
        else
          create_fields = {CarEventId: event_id, Description: desc, DateReported: rep_time}
          create_fields[:Latitude] = get_lat(fields)
          create_fields[:Longitude] = get_lng(fields)
          create_fields[:NumberOfSatellites] = get_num_sat(fields)
          create_fields[:CarDiagnosticCodeId] = fields[:diagnostic_code_id]
          create_fields[:ShortCode] = fields[:diagnostic_code_short_code]

          ceh = Models::CarEventHistory.new(create_fields)
          ceh.car = self
          ceh.save
        end
      end

      def persist_position_event_history(fields)
        if fields[:lat].blank?
          fail DataManager::Errors::MissingMessageData.new(:lat, fields)
        elsif fields[:lng].blank?
          fail DataManager::Errors::MissingMessageData.new(:lng, fields)
        else
          persist_event_history(fields)
        end
      end

      #TODO: Add DateStartPositionModified and DateEndPositionModified to TripSummaries, and then use these dates in conjunction with TripStartTime and TripEndTime to continuously update and improve our
      # trip start pos and trip end pos. This will eliminate all the guessing we have to do between Cars LastKnownGPSCoords and what should be in the TripSummary start and end positions.
      def update_position(fields)
        rep_time = fields[:time]
        lat = get_lat(fields)
        lng = get_lng(fields)
        num_sat = get_num_sat(fields)

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif lat.blank?
          fail DataManager::Errors::MissingMessageData.new(:lat, fields)
        elsif lng.blank?
          fail DataManager::Errors::MissingMessageData.new(:lng, fields)
        else
          lock!

          position_updated = false
          status_updated = false

          #TODO: handle the edge case where we missed the 6011 ignition on event and don't have a trip_summary for this location update
          # the trigger for this is a finalized current_trip_summary and a reported time for this position event AFTER the end time
          # of the current_trip_summary
          # in this case, we should probably make a call to the API in order to retrieve and create the latest trip_summary

          #TODO: handle the edge case where we missed the 6012 ignition off event, so this position update actually belongs to a new trip_summary
          # what's the trigger for this? the reported time of this position update is more than x minutes after the reported time for the last
          # position update?
          # in this case, we should probably make a call to the API in order to retrieve and create the latest trip_summary

          # handles determining if position is more recent than the position we currently have
          # we have to do this before updating the LastKnown values in this car, so that we can tell if this is a better position based on timestamps
          # don't update the last known coordinates if this set of coordinates is older than the one we currently have stored
          if self.DateGPSCoordsModified.blank? || self.DateGPSCoordsModified < rep_time
            update(LastKnownGPSCoords: format_lat_lng(lat, lng), LastNumberOfSatellites: num_sat, DateGPSCoordsModified: rep_time)
            check_for_need_to_update_start_pos_in_trip_summaries(lat, lng, num_sat, rep_time)
            check_for_need_to_update_end_pos_in_trip_summaries(lat, lng, num_sat, rep_time)
            position_updated = true
            status_updated = change_to_driving(rep_time)
          end

          {position_updated: position_updated, status_updated: status_updated}
        end
      end

      #TODO: update only if this message is newer than the one we used to set speed previously
      #TODO: Xirgo: Update to calc and store Avg and Max Speed on trip_summary
      def update_speed(fields)
        updated = false
        rep_time = fields[:time]
        speed = fields[:speed]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif speed.blank?
          fail DataManager::Errors::MissingMessageData.new(:speed, fields)
        else
          if for_current_trip?(rep_time) && speed > 0
          #TODO: make this generic by just finding the trip_summary this speed data point is for, and updating speed on that trip_summary --> do this after TripSummary methods refactored into TripSummary
          #update current trip speeds on the cts iff this speed data point is for the current trip
            cts = self.current_trip_summary
            cts.lock!

            current_avg_speed = cts.AvgSpeed.blank? ? 0 : cts.AvgSpeed
            current_max_speed = cts.MaxSpeed.blank? ? 0 : cts.MaxSpeed
            total_data_points = cts.TotalPointsInAvgSpeed.blank? ? 0 : cts.TotalPointsInAvgSpeed

            update_fields = {}
            update_fields[:AvgSpeed] = cumulative_moving_avg(current_avg_speed, speed, total_data_points)
            update_fields[:TotalPointsInAvgSpeed] = total_data_points + 1
            update_fields[:MaxSpeed] = speed if speed > current_max_speed
            updated = cts.update(update_fields)
          end
        end

        updated
      end

      #TODO: update only if this message is newer than the one we used to set heading previously
      def update_heading(fields)
        #currently we don't store heading for a car, we only forward the message to Event Service if it is for the current_trip_summary
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        else
          for_current_trip?(rep_time)
        end
      end

      def update_speed_and_heading(fields)
        #currently we don't store speed and heading for a car, we only forward the message to Event Service
        update_speed(fields)
        update_heading(fields)
      end

      def update_idling(fields)
        rep_time = fields[:time]
        idling_duration_secs = fields[:idling_duration_secs]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif idling_duration_secs.blank?
          fail DataManager::Errors::MissingMessageData.new(:idling_duration_secs, fields)
        else
          if for_current_trip?(rep_time) && idling_duration_secs > 0
          #TODO: make this generic by just finding the trip_summary this idling data point is for, and updating idling on that trip_summary --> do this after TripSummary methods refactored into TripSummary
          #update current trip idling on the cts iff this idling data point is for the current trip
            #TODO: re-enable this once we have code that sets car status back to driving
            #change_to_idling(rep_time, in_progress)

            cts = self.current_trip_summary
            cts.lock!

            current_idling_secs = cts.IdleTimeSecs.blank? ? 0 : cts.IdleTimeSecs

            update_fields = {}
            update_fields[:IdleTimeSecs] = current_idling_secs + idling_duration_secs
            updated = cts.update(update_fields)
          end

          updated
        end
      end

      def update_ignition(fields)
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        else
          change_to_ignition(rep_time)
        end
      end

      def update_parked(fields)
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        else
          change_to_parked(rep_time)
        end
      end

      def update_odometer(fields)
        updated = false
        rep_time = fields[:time]
        current_mileage_counter = fields[:miles]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif current_mileage_counter.blank?
          fail DataManager::Errors::MissingMessageData.new(:miles, fields)
        # this should only be called when an ignition OFF or Mileage Threshold (Reset) message recv'd meaning we should have the correct current_trip_summary. if not, something is wrong, so fail.
        elsif for_current_trip?(rep_time)
          lock!

          cts = self.current_trip_summary
          if cts
            cts.lock!
            already_counted_trip_miles = cts.TripMiles ? cts.TripMiles : 0 #handle edge case where mileage counter wraps/resets to 0 during trip
            trip_miles = current_mileage_counter - cts.StartMileageCounter
            odo = self.Odometer.blank? ? 0 : self.Odometer
            rep_odo = self.ReportedOdometer.blank? ? 0 : self.ReportedOdometer

            #update Car ReportedOdometer
            update_fields = {}
            if odo < rep_odo
              update_fields[:ReportedOdometer] = rep_odo + trip_miles
            else
              update_fields[:ReportedOdometer] = odo + trip_miles
            end
            updated = update(update_fields)

            #update EndMileageCounter and TripMiles on TripSummary
            update_fields = {}
            update_fields[:TripMiles] = trip_miles + already_counted_trip_miles
            update_fields[:EndMileageCounter] = current_mileage_counter
            updated = cts.update(update_fields) && updated
            #TODO: create an initial oil change reminder if first odo reading (rep_odo was blank???). See CarService.UpdateCarMilesAndTripMPG and CarService.InsertCarOilChangeHistory
            #TODO: check oil change countdown and fire appropriate notification. See CarService.UpdateCarMilesAndTripMPG
          else
            fail DataManager::Errors::NoCurrentTrip.new('Xirgo/DataManager::Models::Cars#update_odometer')
          end
        else
          fail DataManager::Errors::MessageNotForCurrentTrip.new('Xirgo/DataManager::Models::Cars#update_odometer')
        end

        updated
      end

      def reset_mileage_counter!(fields)
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif for_current_trip?(rep_time)
          cts = self.current_trip_summary
          if cts
            cts.lock!
            cts.StartMileageCounter = 0
            cts.save
          else
            fail DataManager::Errors::NoCurrentTrip.new('Xirgo/DataManager::Models::Cars#reset_mileage_counter!')
          end
        end
      end

      def update_avg_mpg(fields)
        updated = false
        rep_time = fields[:time]
        mpg = fields[:fuel_efficiency]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif mpg.blank?
          fail DataManager::Errors::MissingMessageData.new(:fuel_efficiency, fields)
        else
          unless mpg == 0.0
            lock!

            #update car mpg
            current_mpg = self.AvgMPG.blank? ? 0.0 : self.AvgMPG
            total_data_points = self.TotalPointsInMPG.blank? ? 0 : self.TotalPointsInMPG

            update_fields = {}
            update_fields[:AvgMPG] = cumulative_moving_avg(current_mpg, mpg, total_data_points)
            update_fields[:TotalPointsInMPG] = total_data_points + 1
            updated = update(update_fields)

            #TODO: make this generic by just finding the trip_summary this mpg data point is for, and updating mpg on that trip_summary --> do this after TripSummary methods refactored into TripSummary
            #update current trip mpg iff this mpg data point is for the current trip
            if for_current_trip?(rep_time)
              cts = self.current_trip_summary
              cts.lock!

              current_mpg = cts.TripMPG.blank? ? 0.0 : cts.TripMPG
              total_data_points = cts.TotalPointsInMPG.blank? ? 0 : cts.TotalPointsInMPG

              update_fields = {}
              update_fields[:TripMPG] = cumulative_moving_avg(current_mpg, mpg, total_data_points)
              update_fields[:TotalPointsInMPG] = total_data_points + 1
              updated = cts.update(update_fields) && updated
            end
          end
        end

        updated
      end

      #TODO: update only if this message is newer than the one we used to set voltage previously
      def update_voltage(fields)
        voltage = fields[:voltage]
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif voltage.blank?
          fail DataManager::Errors::MissingMessageData.new(:voltage, fields)
        else
          if for_current_trip?(rep_time)
            lock!

            update_fields = {}
            update_fields[:Voltage] = voltage
            update(update_fields)
          else
            false
          end
        end
      end

      #TODO: update only if this message is newer than the one we used to set fuel level previously
      def update_fuel_level(fields)
        fuel_level = fields[:fuel_level]
        rep_time = fields[:time]

        if rep_time.blank?
          fail DataManager::Errors::MissingMessageData.new(:time, fields)
        elsif fuel_level.blank?
          fail DataManager::Errors::MissingMessageData.new(:fuel_level, fields)
        else
          if for_current_trip?(rep_time)
            lock!

            update_fields = {}
            update_fields[:FuelLevel] = fuel_level
            update(update_fields)
          else
            false
          end
        end
      end

      def set_dtc_status_on
        set_dtc_status(true)
      end

      def set_dtc_status_off
        set_dtc_status(false)
      end

      private

      def get_num_sat(fields)
        fields[:num_sat].blank? ? 0 : fields[:num_sat]
      end

      def get_lat(fields)
        fields[:lat].blank? ? 0.0 : fields[:lat]
      end

      def get_lng(fields)
        fields[:lng].blank? ? 0.0 : fields[:lng]
      end

      # handles edge case where we got an ignition event without a LastKnownGPSCoords for this car (see CarService.UpdateCarPosition)
      def check_for_need_to_update_start_pos_in_trip_summaries(lat, lng, num_sat, rep_time)
        summaries = self.trip_summaries_dataset.for_update.where(StartPosLat: 0.0, StartPosLong: 0.0).where('TripStartTime <= :rep_time AND TripEndTime >= :rep_time', rep_time: rep_time)

        summaries.each do |ts|
          ts.update(StartPosLat: lat, StartPosLong: lng, StartPosAccuracy: num_sat)
        end
      end

      # handles edge case where we get a position update for a trip that's ended and has a "final" position, but this new position is more recent than
      # the position we had and used when the trip_summary was set to TripCompleted = true
      def check_for_need_to_update_end_pos_in_trip_summaries(lat, lng, num_sat, rep_time)
        #summaries = self.trip_summaries_dataset.for_update.where(EndPosLat: 0.0, EndPosLong: 0.0).where('TripStartTime <= :rep_time AND TripEndTime >= :rep_time', rep_time: DataManager::ServiceUtils::DateTime.end_time_with_setback(rep_time))
        summaries = self.trip_summaries_dataset.for_update.where('TripStartTime <= :rep_time AND TripEndTime >= :rep_time AND TripCompleted = TRUE', rep_time: DataManager::ServiceUtils::DateTime.end_time_with_setback(rep_time))

        summaries.each do |ts|
          ts.update(EndPosLat: lat, EndPosLong: lng, EndPosAccuracy: num_sat)
        end
      end

      def update_status(status_desc, rep_time)
        lock!

        cts = self.current_trip_summary
        cts.lock! unless cts.blank?
        updated = false

        unless for_old_trip?(rep_time) || old_status?(rep_time) || invalid_state_change?(status_desc, cts, rep_time)
          updated = update(StatusDesc: status_desc, DateStatusModified: rep_time, LastStatusFromTripSummaryId: cts.Id)
        end

        updated
      end

      def update_status!(status_desc, rep_time)
        lock!

        cts = self.current_trip_summary
        cts.lock! unless cts.blank?
        update(StatusDesc: status_desc, DateStatusModified: rep_time, LastStatusFromTripSummaryId: cts.Id)
        true
      end

      # this takes the place of Mavia message 3.3 Driving, because Xirgo does not have an equivalent event message. thus, we make an educated guess here.
      def change_to_driving(rep_time)
        update_status(DataManager::ServiceUtils::Descriptions.vehicle_event_driving, rep_time)
      end

      # here we update status of Car to Idling, iff it is an in-progress event (in_progress == 0)
      def change_to_idling(rep_time, in_progress)
        if in_progress == 0
          update_status(DataManager::ServiceUtils::Descriptions.vehicle_event_idle, rep_time)
        end
      end

      def change_to_ignition(rep_time)
        update_status(DataManager::ServiceUtils::Descriptions.vehicle_event_ignition, rep_time)
      end

      def change_to_parked(rep_time)
        update_status(DataManager::ServiceUtils::Descriptions.vehicle_event_park, rep_time)
      end

      def change_to_parked!(rep_time)
        update_status!(DataManager::ServiceUtils::Descriptions.vehicle_event_park, rep_time)
      end

      def format_lat_lng(lat, lng)
        "#{lat},#{lng}"
      end

      #TODO: I was initially trying to keep state change and old status (time) separate, but in order to keep from updating parked to in-motion when the trip isn't yet marked complete but should be, we need to check that the new gps update that triggered this parked -> in-motion transition isn't within 30 seconds of the last status update, so maybe invalid_state_change? and old_status? should be combined...
      #TODO: Can we make this better/simpler/prettier?
      # TRUE = invalid state transition
      # FALSE = valid state transition
      # STATES = {DataManager::ServiceUtils::Descriptions.vehicle_event_ignition => 1, DataManager::ServiceUtils::Descriptions.vehicle_event_driving => 2, DataManager::ServiceUtils::Descriptions.vehicle_event_idle => 3, DataManager::ServiceUtils::Descriptions.vehicle_event_park => 4}
      # This method is not safe to call concurrently. Anything that calls this should lock cts prior to the call, or face the consequences
      def invalid_state_change?(new_description, cts, status_time)
        #lock!
        #cts.lock! unless cts.blank?

        current_state = STATES[self.StatusDesc]
        new_state = STATES[new_description]

        if cts.blank? # if there's no current trip summary, then the state transition is invalid
          true
        elsif current_state.blank? # if there's a current trip summary, but no current state, then the state transition is valid
          false
        elsif cts.completed? # the current trip is completed, and we don't want to change the status after that, so the transition is invalid
          true
        elsif self.LastStatusFromTripSummaryId != cts.Id # if this is the first recorded status update for a new trip summary, we want to record it no matter what, so the state transition is valid
          false
        else # we have a current state, a current trip summary and an in-progress trip with at least one previous status update on it, so proceed with the validation
          case current_state
          when 1
            case new_state
            when 2, 3, 4
              false
            when 1
              true
            else
              fail DataManager::Errors::InvalidStateDescription("#{self.class}#invalid_state_change?", self.StatusDesc, new_description)
            end
          when 2
            case new_state
            when 3, 4
              false
            when 1, 2
              true
            else
              fail DataManager::Errors::InvalidStateDescription("#{self.class}#invalid_state_change?", self.StatusDesc, new_description)
            end
          when 3
            case new_state
            when 2, 4
              false
            when 1, 3
              true
            else
              fail DataManager::Errors::InvalidStateDescription("#{self.class}#invalid_state_change?", self.StatusDesc, new_description)
            end
          when 4
            case new_state
            when 1, 2, 3
              if for_current_trip?(status_time) # this is a fuzzy guard to attempt to prevent late position updates for a trip causing a transition from parked to another state
              #if DataManager::ServiceUtils::DateTime.end_time_with_setback(status_time) <= self.DateStatusModified
                true
              else
                false
              end
            when 4
              true
            else
              fail DataManager::Errors::InvalidStateDescription("#{self.class}#invalid_state_change?", self.StatusDesc, new_description)
            end
          else
            fail DataManager::Errors::InvalidStateDescription("#{self.class}#invalid_state_change?", self.StatusDesc, new_description)
          end

        end
      end

      # TRUE = trying to do a status update with a status older than the one we currently have
      # FALSE = trying to do a status update with a status newer than the one we currently have
      def old_status?(status_time)
        lock!
        last_modified = self.DateStatusModified

        # the status was last modified before this status, so it's a valid status by time
        if last_modified.blank? || last_modified <= status_time
          false
        else # last_modified > status_time, so it's old
          true
        end
      end

      def for_current_trip?(rep_time)
        lock!

        cts = self.current_trip_summary
        cts.refresh unless cts.blank?

        #if cts.blank? || (cts.TripStartTime && cts.TripStartTime > rep_time) || (cts.TripEndTime && cts.TripEndTime < DataManager::ServiceUtils::DateTime.end_time_with_setback(rep_time))
        if cts.blank? || (cts.TripStartTime && cts.TripStartTime > rep_time) || (cts.TripEndTime && cts.TripEndTime < rep_time)
          false
        else
          true
        end
      end

      def for_old_trip?(rep_time)
        !for_current_trip?(rep_time)
      end

      def set_dtc_status(on = true)
        lock!

        update_fields = {}
        update_fields[:HasActiveDTC] = on
        update(update_fields)
        true
      end

      def cumulative_moving_avg(current_avg, new_data_point, number_of_data_points)
        current_avg + ((new_data_point - current_avg)/(number_of_data_points + 1))
      end

    end
  end
end