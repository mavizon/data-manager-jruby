module DataManager
  module Models
    class TripSummary < Mavia::Models::TripSummary
      include_associations(self)

      def completed?
        self.TripCompleted == 1
      end

    end
  end
end