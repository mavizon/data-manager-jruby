module DataManager

  @logger = Mavia::Logger.instance
  attr_reader :logger
  module_function :logger

  def self.log(msg, opts = {lvl: :info})
    @logger.log(msg, opts)
  end

end