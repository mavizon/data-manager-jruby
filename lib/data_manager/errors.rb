module DataManager
  module Errors
    
    class DataManagerError < StandardError
      
    end
    
    class NoConfigurationMatch < DataManagerError
      def initialize(key)
        @key = key
        super("No matching configuration value for key '#{@key}'")
      end

      attr_reader :key
    end

    class JSONParseError < DataManagerError
      def initialize(error)
        @error = error
        super("Error parsing JSON: #{@error}")
      end

      attr_reader :error
    end

    class XirgoParseError < DataManagerError
      def initialize(error)
        @error = error
        super("Error parsing Xirgo message: #{@error}")
      end

      attr_reader :error
    end

    class JSONEncodeError < DataManagerError
      def initialize(object)
        @object = object
        super("Error encoding object into JSON. Object: #{@object.inspect}")
      end

      attr_reader :object
    end

    class MissingMessageData < DataManagerError
      def initialize(missing_key, message)
        @missing_key = missing_key
        @message = message
        super("Message missing data. Message: #{@message.inspect} Data Missing: #{@missing_key.inspect}")
      end

      attr_reader :missing_key, :message
    end

    class MessageQueueError < DataManagerError
      def initialize(error_location, error)
        @error = error
        @error_location = error_location
        super("Error thrown by the message queue within #{@error_location}: #{@error}")
      end

      attr_reader :error, :error_location
    end

    class DeviceNotFound < DataManagerError
      def initialize(error_location, device_id)
        @device_id = device_id
        @error_location = error_location
        super("Could not find device_id #{@device_id} within #{@error_location}")
      end

      attr_reader :device_id, :error_location
    end

    class DeviceNotActivated < DataManagerError
      def initialize(error_location, device_id)
        @device_id = device_id
        @error_location = error_location
        super("Device inactive error for device_id #{@device_id} within #{@error_location}")
      end

      attr_reader :device_id, :error_location
    end

    class DeviceNotAttachedToCar < DataManagerError
      def initialize(error_location, device_id)
        @device_id = device_id
        @error_location = error_location
        super("Received data for Device without a Car attached: device_id #{@device_id} within #{@error_location}")
      end

      attr_reader :device_id, :error_location
    end

    class UnableToAssociateDeviceWithCar < DataManagerError
      def initialize(error_location, device_id)
        @device_id = device_id
        @error_location = error_location
        super("Unable to automatically associate this device with a car: device_id #{@device_id} within #{@error_location}")
      end

      attr_reader :device_id, :error_location
    end


    class DeviceNotAttachedToUser < DataManagerError
      def initialize(error_location, device_id)
        @device_id = device_id
        @error_location = error_location
        super("Received data for Device without a User attached: device_id #{@device_id} within #{@error_location}")
      end

      attr_reader :device_id, :error_location
    end
    
    class UnknownMessageType < DataManagerError
      def initialize(error_location, message_type)
        @message_type = message_type
        @error_location = error_location
        super("Received an unknown Message Type (Action Code): message_type #{@message_type} within #{@error_location}")
      end

      attr_reader :message_type, :error_location
    end

    class UnknownIgnitionType < DataManagerError
      def initialize(error_location, ignition_type)
        @ignition_type = ignition_type
        @error_location = error_location
        super("Received an unknown Ignition Type #{@ignition_type} within #{@error_location}")
      end

      attr_reader :ignition_type, :error_location
    end

    class InvalidTripId < DataManagerError
      def initialize(error_location, message)
        @error_location = error_location
        @message = message
        super("Received an event with a trip_id that doesn't make sense, so ignored it. Message: #{@message.inspect} within #{@error_location}")
      end

      attr_reader :error_location, :message
    end

    class InvalidClass < DataManagerError
      def initialize(error_location, expected, actual)
        @error_location = error_location
        @expected = expected
        @actual = actual
        super("Expected a #{@expected}, but got a #{actual} at #{@error_location}")
      end

      attr_reader :error_location, :expected, :actual
    end

    class InvalidApiResponse < DataManagerError
      def initialize(error_location, endpoint, response)
        @error_location = error_location
        @end_point = endpoint
        @response = response
        super("Invalid API response from endpoint #{@end_point} at #{@error_location}. Response: #{@response}")
      end

      attr_reader :error_location, :end_point, :response
    end

    class InvalidStateDescription < DataManagerError
      def initialize(error_location, current_state, new_state)
        @error_location = error_location
        @current_state = current_state
        @new_state = new_state
        super("Invalid state found at #{@error_location}. Current State #{@current_state}. New state: #{@new_state}.")
      end

      attr_reader :error_location, :current_state, :new_state
    end

    class InvalidDiagnosticShortCode < DataManagerError
      def initialize(missing_code, message)
        @missing_code = missing_code
        @message = message
        super("Message tried to create DTC with invalid short code. Message: #{@message.inspect} Data Missing: #{@missing_code}")
      end

      attr_reader :missing_code, :message
    end

    class MessageNotForCurrentTrip < DataManagerError
      def initialize(error_location)
        @error_location = error_location
        super("Attempted to update current trip with data not for current trip. Error location: #{@error_location}")
      end

      attr_reader :error_location
    end

    class NoCurrentTrip < DataManagerError
      def initialize(error_location)
        @error_location = error_location
        super("Attempted to update current trip without a current trip. Error location: #{@error_location}")
      end

      attr_reader :error_location
    end

    class InvalidFieldType < DataManagerError
      def initialize(expected, got, error_location)
        @expected = expected
        @got = got
        @error_location = error_location
        super("Invalid Field Type in incoming message. Expected #{@expected}, but got #{@got}. Error location: #{@error_location}")
      end

      attr_reader :expected, :got, :error_location
    end

  end
end