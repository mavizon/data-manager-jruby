module DataManager
  module Messages
    module Mixins
      module UpdatesTripSummary

        def start_trip_summary(message)
          device.start_trip_summary(message)
        end

        def finish_trip_summary(message)
          device.finish_trip_summary(message)
        end

        def update_trip_summary(message)
          device.update_trip_summary(message)
        end

      end
    end
  end
end
