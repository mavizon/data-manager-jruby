module DataManager
  module Messages
    module Mixins
      module UpdatesCar
        include Mavia::Mixins::UsesNotificationService

        JSON = DataManager::ServiceUtils::Encoders::JSON

        private

        #TODO: re-visit use of method_missing here, as well as clean it up and implement respond_to? if we keep method_missing

        def method_missing(method_name, *args)
          begin
            our_method = method("_#{method_name}".to_sym)
          rescue
            our_method = nil
          end

          if our_method
            begin
              #puts args
              associate_with_car_if_no_car(args[0])
              notify = args[1].nil? ? true : args[1]
              our_method.call(args[0], notify)
            rescue
              raise
            end
          else
            super
          end
        end

        def _update_car_position(parsed_message, notify = true)
          updated = device.car.update_position(parsed_message)
          #puts notify
          if notify
            #puts updated
            #puts notify
            #add_to_notifications_queue(:position_updated) if updated[:position_updated]
            #add_to_notifications_queue(:car_driving) if updated[:status_updated]
          end
        end

        def _update_car_position_history(parsed_message, notify = true)
          #add_to_notifications_queue(:gps_history) if notify
        end

        def _update_car_speed(parsed_message, notify = true)
          updated = device.car.update_speed(parsed_message)
          #add_to_notifications_queue(:speed_updated) if notify && updated
        end

        def _update_car_heading(parsed_message, notify = true)
          updated = device.car.update_heading(parsed_message)
          #add_to_notifications_queue(:heading_updated) if notify && updated
        end

        def _update_car_idling(parsed_message, notify = true)
          updated = device.car.update_idling(parsed_message)
          #add_to_notifications_queue(:car_status_idling) if notify && updated
        end

        def _update_car_ignition(parsed_message, notify = true)
          updated = device.car.update_ignition(parsed_message)
          #add_to_notifications_queue(:car_status_ignition) if notify && updated
        end

        def _update_car_parked(parsed_message, notify = true)
          updated = device.car.update_parked(parsed_message)
          #add_to_notifications_queue(:car_status_parked) if notify && updated
        end

        def _update_car_parked!(parsed_message, notify = true)
          updated = device.car.update_parked(parsed_message)
          #add_to_notifications_queue(:car_status_parked) if notify && updated
        end

        def _update_car_odometer(parsed_message, notify = true)
          updated = device.car.update_odometer(parsed_message)
          #add_to_notifications_queue(:odometer_updated) if notify && updated
        end

        def _update_car_voltage(parsed_message, notify = true)
          updated = device.car.update_voltage(parsed_message)
          #add_to_notifications_queue(:voltage_updated) if notify && updated
        end

        def _update_car_fuel_level(parsed_message, notify = true)
          updated = device.car.update_fuel_level(parsed_message)
          #add_to_notifications_queue(:fuel_level_updated) if notify && updated
        end

        def _set_car_dtc_status_on(parsed_message, notify = true)
          updated = device.car.set_dtc_status_on
          if notify && updated
            ndid = DataManager.opts[:notification_definitions][:dtc]
            template_data = {car: {model: device.car.car_model_name, make: device.car.car_make_name, trouble_code: {description: parsed_message[:description]}}}
            alert_message = {user_id: device.UserId, car_id: device.CarId, notification_definition_id: ndid, template_data: template_data}
            add_to_notifications_queue(JSON.encode(alert_message))
          end
        end

        def _set_car_dtc_status_off(parsed_message, notify = true)
          updated = device.car.set_dtc_status_off
          #add_to_notifications_queue(:dtc_alert_cleared) if notify && updated
        end

        def associate_with_car_if_no_car(parsed_message)
          unless device.has_car?
            associate_with_car(parsed_message)
          end
        end

        #This needs to be implemented in any service that uses UpdatesCar
        def associate_with_car(parsed_message)
          DataManager.log 'Unimplemented associate_with_car!', lvl: :error
        end

      end
    end
  end
end