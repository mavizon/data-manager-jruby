module DataManager
  module Messages
    module Mixins
      module StoresEventHistory

        private

        def store_device_event_history(parsed_message, raw_message)
          b64_raw_message = DataManager::ServiceUtils::Encoders::Base64.encode(raw_message)
          device.persist_event_history(parsed_message, b64_raw_message)
        end

        def store_car_position_event_history(parsed_message)
          if device.car.blank?
            DataManager.log DataManager::Errors::DeviceNotAttachedToCar.new(@message_type, device_id)
          else
            device.car.persist_position_event_history(parsed_message)
          end
        end

        def store_car_event_history(parsed_message)
          if device.car.blank?
            DataManager.log DataManager::Errors::DeviceNotAttachedToCar.new(@message_type, device_id)
          else
            device.car.persist_event_history(parsed_message)
          end
        end

      end
    end
  end
end

