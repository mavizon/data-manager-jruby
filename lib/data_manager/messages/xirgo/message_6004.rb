module DataManager
  module Messages
    module Xirgo
      class Message6004 < MajorityBase
        # 6004: Geofence crossing alert
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either

        def initialize(raw_message, message_field_array)
          super(:'6004', raw_message, message_field_array)
        end

        private

        # Fuel Level is in a different position in this message, so over-riding fuel_level in MajorityBase
        def fuel_level
          if @fuel_level
            @fuel_level
          else
            if @message_field_array[21].blank?
              fail DataManager::Errors::MissingMessageData.new(:fuel_level, @raw_message)
            else
              @fuel_level = @message_field_array[21].to_f
            end
          end
        end

      end
    end
  end
end
