module DataManager
  module Messages
    module Xirgo
      class Message4001 < MajorityBase
        # 4001: Periodic location reporting with ignition ON

        def initialize(raw_message, message_field_array)
          super(:'4001', raw_message, message_field_array)
        end

      end
    end
  end
end