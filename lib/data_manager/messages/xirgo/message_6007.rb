module DataManager
  module Messages
    module Xirgo
      class Message6007 < MajorityBase
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either
        # 6007: Deceleration Alert

        def initialize(raw_message, message_field_array)
          super(:'6007', raw_message, message_field_array)
        end

      end
    end
  end
end