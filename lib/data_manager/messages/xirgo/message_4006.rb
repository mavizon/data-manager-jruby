require_relative 'heartbeat_message'

module DataManager
  module Messages
    module Xirgo
      class Message4006 < HeartbeatMessage
        # 4006: Periodic location reporting while device powered (heartbeat)

        def initialize(raw_message, message_field_array)
          super(:'4006', raw_message, message_field_array)
        end

      end
    end
  end
end