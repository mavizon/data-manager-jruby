module DataManager
  module Messages
    module Xirgo
      class Message6008 < MajorityBase
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either
        # 6008: Battery threshold alert

        def initialize(raw_message, message_field_array)
          super(:'6008', raw_message, message_field_array)
        end

      end
    end
  end
end