require_relative 'mixins/mixins'

module DataManager
  module Messages
    module Xirgo
      class Base
        include Mavia::Models::Mixins::HasTransaction
        include Mavia::Mixins::UsesNotificationService
        include DataManager::Messages::Xirgo::Mixins::HasDevice
        include DataManager::Messages::Xirgo::Mixins::HasMessageType

        def self.message_factory(raw_message)
          messages = self.parse(raw_message)
          message_classes = []

          messages.each do |message_fields|
            if message_fields[1]
              message_class_name = "Message#{message_fields[1]}"
              if DataManager::Messages::Xirgo.const_defined?(message_class_name)
                message_class = DataManager::Messages::Xirgo.const_get(message_class_name)
                message_classes << message_class.new(raw_message, message_fields)
              else
                DataManager.log DataManager::Errors::UnknownMessageType.new('Messages::Xirgo::Base.message_factory', message_class_name), lvl: :error
              end
            else
              DataManager.log DataManager::Errors::UnknownMessageType.new('Messages::Xirgo::Base.message_factory', message_class_name), lvl: :error
            end
          end

          message_classes
        end

        def initialize(message_type, raw_message, message_field_array)
          @raw_message = raw_message
          @message_field_array = message_field_array
          @service = :xirgo
          @message_type = message_type
          @messages_for_downstream = []
          @base_message = { service: @service,
                            type: @message_type,
                            device_id: device_id,
                            mavia_device_id: mavia_device_id }
        end

        attr_reader :message_field_array

        def execute
          handle_messages
          DataManager.log "Messages for downstream: #{@messages_for_downstream.inspect}"
          persist_to_db
        end

        def hello
          p @messages_for_downstream
        end

        private

        def self.parse(raw_message)
          DataManager::ServiceUtils::Parsers::Xirgo.parse(raw_message)
        end

        def parse
          begin
            @message_hash ||= self.class.parse(@raw_message)
          rescue => e
            DataManager.log e, level: :error, error: e
            raise
          end
        end

        def persist_to_db
          begin
            wrap_in_serializable_transaction do
              add_after_commit_callback_to_transaction(&method(:forward_notifications_to_notification_service)) if respond_to?(:forward_notifications_to_notification_service)
              persist_to_db_logic
            end
          rescue => e
            DataManager.log "Error in Messages::Xirgo::Base#persist_to_db. Error: #{e}. Raw message: #{@raw_message}.", level: :error, error: e
            clear_notifications_queue if respond_to?(:clear_notifications_queue)
            raise
          end
        end

        def handle_messages
          #Must implement this method in child classes
          DataManager.log 'Called unimplemented method handle_messages.', lvl: :error
        end

        def persist_to_db_logic
          #Must implement this method in child classes
          DataManager.log 'Called unimplemented method persist_to_db_logic.', lvl: :error
        end

      end
    end
  end
end