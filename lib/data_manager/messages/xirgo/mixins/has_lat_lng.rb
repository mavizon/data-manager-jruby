module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasLatLng

          private

          def lat
            if @lat
              @lat
            else
              if @message_field_array[4].blank?
                fail DataManager::Errors::MissingMessageData.new(:lat, @raw_message)
              else
                @lat = @message_field_array[4].to_f
              end
            end
          end

          def lng
            if @lng
              @lng
            else
              if @message_field_array[5].blank?
                fail DataManager::Errors::MissingMessageData.new(:lng, @raw_message)
              else
                @lng = @message_field_array[5].to_f
              end
            end
          end

        end
      end
    end
  end
end