module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasHeading

          private

          def heading
            if @heading
              @heading
            else
              if @message_field_array[11].blank?
                fail DataManager::Errors::MissingMessageData.new(:heading, @raw_message)
              else
                @heading = @message_field_array[11].to_f
              end
            end
          end

        end
      end
    end
  end
end