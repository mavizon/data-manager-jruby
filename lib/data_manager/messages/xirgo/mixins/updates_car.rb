module DataManager
  module Messages
    module Mixins
      module UpdatesCar

        private

        def _update_car_mpg(parsed_message, notify = true)
          updated = device.car.update_avg_mpg(parsed_message)
        end

        def _reset_car_mileage_counter!(parsed_message, notify = false)
          updated = device.car.reset_mileage_counter!(parsed_message)
        end

        def associate_with_car(parsed_message)
          begin
            device.associate_with_car(parsed_message, @raw_message)
          rescue => e
            if e.is_a?(DataManager::Errors::UnableToAssociateDeviceWithCar)
              #add_to_notifications_queue(:could_not_associate_device_with_car)
              #forward_notifications_to_notification_service
            end
            raise
          end
        end

      end
    end
  end
end