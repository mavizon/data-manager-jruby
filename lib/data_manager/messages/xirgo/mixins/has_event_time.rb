module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasEventTime

          private

          def event_time
            if @event_time
              @event_time
            else
              if @message_field_array[2].blank? || @message_field_array[3].blank?
                fail DataManager::Errors::MissingMessageData.new(:event_time, @raw_message)
              else
                time = "#{@message_field_array[2]} #{@message_field_array[3]} +0000"
                @event_time = Time.parse(time).utc
              end
            end
          end

        end
      end
    end
  end
end