module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasSpeed

          private

          def speed
            if @speed
              @speed
            else
              if @message_field_array[7].blank?
                fail DataManager::Errors::MissingMessageData.new(:speed, @raw_message)
              else
                @speed = @message_field_array[7].to_i
              end
            end
          end

        end
      end
    end
  end
end