module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasFuelEfficiency

          private

          def fuel_efficiency
            if @fuel_efficiency
              @fuel_efficiency
            else
              if @message_field_array[15].blank?
                fail DataManager::Errors::MissingMessageData.new(:fuel_efficiency, @raw_message)
              else
                @fuel_efficiency = @message_field_array[15].to_f
              end
            end
          end

        end
      end
    end
  end
end