module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasMessageType

          private

          def message_type
            if @message_type
              @message_type
            else
              if @message_field_array[1].blank?
                fail DataManager::Errors::MissingMessageData.new(:message_type, @raw_message)
              else
                @message_type = @message_field_array[1].dehumanized_sym
              end
            end
          end

        end
      end
    end
  end
end