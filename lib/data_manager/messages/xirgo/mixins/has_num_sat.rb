module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasNumSat

          private

          def num_sat
            if @num_sat
              @num_sat
            else
              if @message_field_array[12].blank?
                fail DataManager::Errors::MissingMessageData.new(:num_sat, @raw_message)
              else
                @num_sat = @message_field_array[12].to_i
              end
            end
          end

        end
      end
    end
  end
end