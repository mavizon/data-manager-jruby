module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasMileage

          private

          def mileage
            if @mileage
              @mileage
            else
              if @message_field_array[14].blank?
                fail DataManager::Errors::MissingMessageData.new(:mileage, @raw_message)
              else
                @mileage = @message_field_array[14].to_f
              end
            end
          end

        end
      end
    end
  end
end