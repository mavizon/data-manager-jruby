module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasVIN

          private

          def vin
            if @vin
              @vin
            else
              if @message_field_array[19].blank?
                fail DataManager::Errors::MissingMessageData.new(:vin, @raw_message)
              else
                @vin = @message_field_array[19]
              end
            end
          end

        end
      end
    end
  end
end