module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasBatteryVoltage

          private

          def battery_voltage
            if @battery_voltage
              @battery_voltage
            else
              if @message_field_array[16].blank?
                fail DataManager::Errors::MissingMessageData.new(:battery_voltage, @raw_message)
              else
                @battery_voltage = @message_field_array[16].to_f
              end
            end
          end

        end
      end
    end
  end
end