module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasDevice

          private

          def device
            if @device
              @device
            else
              unless device_id.blank?
                @device = DataManager::Models::Device[XirgoUID: device_id]
                if @device.blank?
                  fail DataManager::Errors::DeviceNotFound.new(@message_type, @device_id)
                elsif !@device.IsMaviaActivated
                  fail DataManager::Errors::DeviceNotActivated.new(@message_type, @device_id)
                end

                @device
              end
            end
          end

          def device_id
            if @device_id
              @device_id
            else
              if @message_field_array[0].blank?
                fail DataManager::Errors::MissingMessageData.new(:device_id, @raw_message)
              else
                device_id_int = @message_field_array[0].to_i
                if device_id_int == 0 #device_id in message is a String, which is an error
                  fail DataManager::Errors::InvalidFieldType.new('Integer', 'String', 'DataManager::Messages::Xirgo::Mixins::HasDevice#device_id')
                else
                  @device_id = device_id_int
                end
              end
              @device_id
            end
          end

          def mavia_device_id
            if @mavia_device_id
              @mavia_device_id
            else
              if device.Id.blank?
                fail DataManager::Errors::MissingMessageData.new(:mavia_device_id, @raw_message)
              else
                @mavia_device_id = device.Id
              end
              @mavia_device_id
            end
          end

        end
      end
    end
  end
end