module DataManager
  module Messages
    module Xirgo
      module Mixins
        module HasFuelLevel

          private

          def fuel_level
            if @fuel_level
              @fuel_level
            else
              if @message_field_array[20].blank?
                fail DataManager::Errors::MissingMessageData.new(:fuel_level, @raw_message)
              else
                @fuel_level = @message_field_array[20].to_i
              end
            end
          end

        end
      end
    end
  end
end