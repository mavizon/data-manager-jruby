module DataManager
  module Messages
    module Xirgo
      class MajorityBase < Base
        include DataManager::Messages::Xirgo::Mixins::HasEventTime
        include DataManager::Messages::Xirgo::Mixins::HasLatLng
        include DataManager::Messages::Xirgo::Mixins::HasNumSat
        include DataManager::Messages::Xirgo::Mixins::HasSpeed
        include DataManager::Messages::Xirgo::Mixins::HasHeading
        include DataManager::Messages::Xirgo::Mixins::HasMileage
        include DataManager::Messages::Xirgo::Mixins::HasFuelEfficiency
        include DataManager::Messages::Xirgo::Mixins::HasBatteryVoltage
        include DataManager::Messages::Mixins::StoresEventHistory
        include DataManager::Messages::Mixins::UpdatesCar
        include DataManager::Messages::Mixins::UpdatesTripSummary
        include Mavia::Mixins::UsesGeofenceService

        def initialize(message_type, raw_message, message_field_array)
          super(message_type, raw_message, message_field_array)
          #TODO: relevant fields should be added to base_message in mixins, not here
          @base_message.merge!(
              {
                lat: lat,
                lng: lng,
                time: event_time,
                num_sat: num_sat,
                speed: speed,
                heading: heading,
                miles: mileage,
                fuel_efficiency: fuel_efficiency,
                fuel_level: fuel_level,
                voltage: battery_voltage
              }
          )
        end

        private

        def fuel_level
          if @fuel_level
            @fuel_level
          else
            if @message_field_array[20].blank?
              fail DataManager::Errors::MissingMessageData.new(:fuel_level, @raw_message)
            else
              @fuel_level = @message_field_array[20].to_f
            end
          end
        end

        def handle_messages
          messages_for_downstream
        end

        #TODO: I'm not loving this. Want to keep creating the messages_for_downstream and persisting to db decoupled so we can scale or change them independently, but I do not at all like the case statement here. How can this be made better?? --> Maybe another set of message classes: Mavia message classes that represent the message hashes in @messages_for_downstream as well as persist_to_db logic for themselves. Would also want a to_hash method on each of them.
        def persist_to_db_logic
          @messages_for_downstream.each do |message|
            #here we yield to the block passed in from the child persist_to_db_logic method, which handles persisting to db for messages in @messages_for_downstream
            #specific to that child message. the block returns true if it handled the persistence for this iteration and false if it did not. If the block doesn't handle
            #the persistence for this message, then try the base clauses, otherwise, loop to next message in @messages_for_downstream

            unless block_given? && yield(message)
              case message[:device_event_id]
              #location/position
              when DataManager.opts[:device_event_ids][:position_message]
                store_device_event_history(message, @raw_message)
                store_car_position_event_history(message)
                update_car_position(message)
                forward_message_to_geofence_service(DataManager::ServiceUtils::Encoders::JSON.encode(message))
              #speed and heading
              when DataManager.opts[:device_event_ids][:speed_heading_event_message]
                store_device_event_history(message, @raw_message)
                update_car_speed(message)
                update_car_heading(message)
              #battery voltage and fuel level
              when DataManager.opts[:device_event_ids][:status_message]
                store_device_event_history(message, @raw_message)

                case message[:car_event_id]
                when DataManager.opts[:car_event_ids][:battery_voltage_update]
                  update_car_voltage(message)
                when DataManager.opts[:car_event_ids][:fuel_level_update]
                  update_car_fuel_level(message)
                end
              end
            end
          end

          #update mpg using just the first message, as mpg is duplicated in each of the messages in @messages_for_downstream
          update_car_mpg(@messages_for_downstream.first)
        end

        def messages_for_downstream
          #TODO: add peer_token to messages for security?

          #here we yield to the block passed in from the child messages_for_downstream method, which will insert messages specific for that child message class into @messages_for_downstream
          yield if block_given?

          #location update
          message = @base_message.dup
          message[:car_event_id] = DataManager.opts[:car_event_ids][:location_update]
          message[:device_event_id] = DataManager.opts[:device_event_ids][:position_message]
          message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_param_position(lat, lng)
          @messages_for_downstream << message

          #speed and heading update
          message = @base_message.dup
          message[:car_event_id] = ''
          message[:device_event_id] = DataManager.opts[:device_event_ids][:speed_heading_event_message]
          message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_param_speed_heading(speed, heading)
          @messages_for_downstream << message

          #battery voltage update
          message = @base_message.dup
          message[:car_event_id] = DataManager.opts[:car_event_ids][:battery_voltage_update]
          message[:device_event_id] = DataManager.opts[:device_event_ids][:status_message]
          message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_event_param_voltage(battery_voltage)
          @messages_for_downstream << message

          #battery fuel level update
          message = @base_message.dup
          message[:car_event_id] = DataManager.opts[:car_event_ids][:fuel_level_update]
          message[:device_event_id] = DataManager.opts[:device_event_ids][:status_message]
          message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_event_param_fuel(fuel_level)
          @messages_for_downstream << message
        end

      end
    end
  end
end
