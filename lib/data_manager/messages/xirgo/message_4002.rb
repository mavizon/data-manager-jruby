module DataManager
  module Messages
    module Xirgo
      class Message4002 < MajorityBase
        # 4002: Periodic location reporting with ignition OFF

        def initialize(raw_message, message_field_array)
          super(:'4002', raw_message, message_field_array)
        end

        private

        # override parent persist_to_db_logic because we only want to store the car's position and make sure the car's status description is parked (since this message is only received when the car's ignition is OFF we know it is not moving). also, we don't want any notifications to be sent on this message. we use the first message in @messages_for_downstream because all relevant data is the same in all messages
        def persist_to_db_logic
          update_car_position(@messages_for_downstream.first, false)
          update_car_parked!(@messages_for_downstream.first, false)
        end

      end
    end
  end
end
