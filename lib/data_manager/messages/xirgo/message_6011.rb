module DataManager
  module Messages
    module Xirgo
      class Message6011 < MajorityBase
        # 6011: Ignition ON alert

        def initialize(raw_message, message_field_array)
          super(:'6011', raw_message, message_field_array)
        end

        private

        # Fuel Level is in a different position in this message, so over-riding fuel_level in MajorityBase
        def fuel_level
          if @fuel_level
            @fuel_level
          else
            if @message_field_array[19].blank?
              fail DataManager::Errors::MissingMessageData.new(:fuel_level, @raw_message)
            else
              @fuel_level = @message_field_array[19].to_f
            end
          end
        end

        def handle_messages
          messages_for_downstream
        end

        def persist_to_db_logic

          #we do this first in order to ensure we have a trip properly started; we use the first message in @messages_for_downstream because all relevant data is the same in all messages
          start_trip_summary(@messages_for_downstream.first)

          super do |message|
            #ignition ON
            if message[:device_event_id] == DataManager.opts[:device_event_ids][:ignition_message]
              #TODO: we can just pass message here, right??
              update_car_ignition(@messages_for_downstream.first)
              store_device_event_history(message, @raw_message)
              store_car_event_history(message)
              true
            else
              false
            end
          end
        end

        def messages_for_downstream
          super do
            #ignition ON event
            message = @base_message.dup
            message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_event_ignition
            message[:device_event_id] = DataManager.opts[:device_event_ids][:ignition_message]
            message[:car_event_id] = DataManager.opts[:car_event_ids][:state_change]
            @messages_for_downstream << message
          end
        end

      end
    end
  end
end
