module DataManager
  module Messages
    module Xirgo
      class Message6005 < MajorityBase
        # 6005: Mileage threshold alert is exceeded

        def initialize(raw_message, message_field_array)
          super(:'6005', raw_message, message_field_array)
        end

        private

        #TODO: This is not working correctly!
        def persist_to_db_logic
          #update_car_odometer(@messages_for_downstream.first, false)
          #reset_car_mileage_counter!(@messages_for_downstream.first, false)
        end

      end
    end
  end
end