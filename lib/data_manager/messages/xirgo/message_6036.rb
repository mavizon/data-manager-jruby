module DataManager
  module Messages
    module Xirgo
      class Message6036 < MajorityBase
        # 6036: Speed Band Duration alert
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either

        def initialize(raw_message, message_field_array)
          super(:'6036', raw_message, message_field_array)
        end

      end
    end
  end
end
