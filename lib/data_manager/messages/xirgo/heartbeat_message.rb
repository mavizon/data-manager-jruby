module DataManager
  module Messages
    module Xirgo
      class HeartbeatMessage < MajorityBase
        include DataManager::Messages::Xirgo::Mixins::HasVIN
        include DataManager::Messages::Mixins::UpdatesCar

        def initialize(message_type, raw_message, message_field_array)
          super(message_type, raw_message, message_field_array)
          @base_message.merge!(
              {
                vin: vin
              }
          )
        end

        private

        # Fuel Level is in a different position in this message, so over-riding fuel_level in MajorityBase
        def fuel_level
          if @fuel_level
            @fuel_level
          else
            if @message_field_array[24].blank?
              fail DataManager::Errors::MissingMessageData.new(:fuel_level, @raw_message)
            else
              @fuel_level = @message_field_array[24].to_f
            end
          end
        end

        # for now, we don't do anything with heartbeat messages except use them to associate a device and a car
        # heartbeat messages have VIN, so whenever we get one, we try to associate with a car; we use the first message in @messages_for_downstream because all relevant data is the same in all messages
        def persist_to_db_logic
          associate_with_car(@messages_for_downstream.first)
        end
      end
    end
  end
end
