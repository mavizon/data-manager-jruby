require_relative 'heartbeat_message'

module DataManager
  module Messages
    module Xirgo
      class Message6015 < HeartbeatMessage
        # 6015: Alert message on Power up/Reset and GPS lock

        def initialize(raw_message, message_field_array)
          super(:'6015', raw_message, message_field_array)
        end

      end
    end
  end
end
