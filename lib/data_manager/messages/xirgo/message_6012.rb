module DataManager
  module Messages
    module Xirgo
      class Message6012 < MajorityBase
        # 6012: Ignition OFF alert
        include DataManager::Messages::Mixins::UpdatesTripSummary

        def initialize(raw_message, message_field_array)
          super(:'6012', raw_message, message_field_array)
        end

        private

        def handle_messages
          messages_for_downstream
        end

        def persist_to_db_logic
          super do |message|
            #ignition OFF
            if message[:device_event_id] == DataManager.opts[:device_event_ids][:park_message]
              update_car_parked(message)
              store_device_event_history(message, @raw_message)
              store_car_event_history(message)
              true
            else
              false
            end
          end

          #update final odo using just the first message, as odo is duplicated in each of the messages in @messages_for_downstream
          update_car_odometer(@messages_for_downstream.first)
          finish_trip_summary(@messages_for_downstream.first)
        end

        def messages_for_downstream
          super do
            #ignition OFF event
            message = @base_message.dup
            message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_event_park
            message[:device_event_id] = DataManager.opts[:device_event_ids][:park_message]
            message[:car_event_id] = DataManager.opts[:car_event_ids][:state_change]
            @messages_for_downstream << message
          end
        end

      end
    end
  end
end