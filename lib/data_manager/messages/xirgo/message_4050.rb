module DataManager
  module Messages
    module Xirgo
      class Message4050 < Base
      # 4050: Diagnostic Data (Reference 7050 command for field definitions)

        def initialize(raw_message, message_field_array)
          super(:'4050', raw_message, message_field_array)
        end

        private

        def handle_messages
          keys = "<UID>,<MSG>,<REG>,<HOFF>,<HO>,<PU>,<R>,<%GPS>,<%GPSQ>,<%REG>,<LV>,<HV>,<DBO>,<ABI>,<DBI>,<SO>,<SI>,<SS>".gsub('<','').gsub('>','').split(',')

          @messages_for_downstream << DataManager::ServiceUtils::Encoders::JSON.encode(Hash[keys.zip(@message_field_array)])
        end

        def persist_to_db_logic
          DataManager.log @messages_for_downstream.first
        end

      end
    end
  end
end
