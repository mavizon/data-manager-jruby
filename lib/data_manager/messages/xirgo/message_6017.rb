module DataManager
  module Messages
    module Xirgo
      class Message6017 < MajorityBase
        #TODO: send out towing alert
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either
        # 6017: Towing Start alert

        def initialize(raw_message, message_field_array)
          super(:'6017', raw_message, message_field_array)
        end

      end
    end
  end
end