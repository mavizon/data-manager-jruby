module DataManager
  module Messages
    module Xirgo
      class Message6045 < Base
        # 6045: Diagnostic Trouble Code List Change Alert
        #This cannot inherit from MajorityBase because it doesn't have speed, heading, mileage, fuel efficiency, or battery voltage
        include Mavia::Models::Mixins::UsesDiagnosticCodes
        include DataManager::Messages::Mixins::StoresEventHistory
        include DataManager::Messages::Mixins::UpdatesCar
        include DataManager::Messages::Xirgo::Mixins::HasEventTime
        include DataManager::Messages::Xirgo::Mixins::HasLatLng
        #this has num_sat, but it is in position 7 instead of 12 so we define num_sat below instead of use the mix-in

        def initialize(raw_message, message_field_array)
          super(:'6045', raw_message, message_field_array)
        end

        private

        def handle_messages
          handle_mil
          handle_dtc_list
        end

        def persist_to_db_logic
          if dtc_count == 0
            set_car_dtc_status_off(@messages_for_downstream.first)
          else
            set_car_dtc_status_on(@messages_for_downstream.first)
          end

          @messages_for_downstream.each do |message|
            begin
              store_car_event_history(message)
              store_device_event_history(message, @raw_message)
            rescue DataManager::Errors::DataManagerError => e
              DataManager.log "#{e}", lvl: :error, error: e
            end
          end
        end

        def message_for_downstream(device_event_id, car_event_id, description, dtc_short_code, dtc_id = nil)
          message = {service: @service, type: @message_type, device_id: device_id, device_event_id: device_event_id, car_event_id: car_event_id, time: event_time, description: description, diagnostic_code_short_code: dtc_short_code}
          message[:diagnostic_code_id] = dtc_id if dtc_id

          message
        end

        #can't use HasNumSat because it is in a different location in this message
        def num_sat
          if @num_sat
            @num_sat
          else
            if @message_field_array[7].blank?
              fail DataManager::Errors::MissingMessageData.new(:num_sat, @raw_message)
            else
              @num_sat = @message_field_array[7].to_i
            end
          end
        end

        def mil_status
          if @mil_status
            @mil_status
          else
            if @message_field_array[11].blank?
              fail DataManager::Errors::MissingMessageData.new(:mil_status, @raw_message)
            else
              @mil_status = @message_field_array[11].to_i
            end
          end
        end

        def dtc_count
          if @dtc_count
            @dtc_count
          else
            if @message_field_array[12].blank?
              fail DataManager::Errors::MissingMessageData.new(:dtc_count, @raw_message)
            else
              @dtc_count = @message_field_array[12].to_i
            end
          end
        end

        def dtc_list
          if @dtc_list
            @dtc_list
          else
            @dtc_list = @message_field_array[13, dtc_count]
            if @dtc_list.blank? || @dtc_list.length != dtc_count
              fail DataManager::Errors::MissingMessageData.new(:dtc, @raw_message)
            else
              @dtc_list
            end
          end
        end

        def handle_mil
          cdc = find_diagnostic_code_by_short_code(DataManager.opts[:diagnostic_trouble_codes][:check_engine_light])

          if cdc.blank?
            fail DataManager::Errors::InvalidDiagnosticShortCode.new(DataManager.opts[:diagnostic_trouble_codes][:check_engine_light], @raw_message)
          else
            device_event_id = DataManager.opts[:device_event_ids][:check_engine_event_message]
            car_event_id = DataManager.opts[:car_event_ids][:trouble_code_found]
            if mil_status == 1 #indicator on
              description = DataManager::ServiceUtils::Descriptions.vehicle_event_cel(cdc.FriendlyDesc)
            else #indicator off
              description = DataManager::ServiceUtils::Descriptions.vehicle_event_cel_off
            end
            @messages_for_downstream << message_for_downstream(device_event_id, car_event_id, description, cdc.ShortCode, cdc.Id)
          end
        end

        def handle_dtc_list

          # handle dtc list
          if dtc_count == 0 # indicates all DTC alerts have cleared
            device_event_id = DataManager.opts[:device_event_ids][:trouble_code_event_message]
            car_event_id = DataManager.opts[:car_event_ids][:trouble_code_found]
            description = DataManager::ServiceUtils::Descriptions.vehicle_event_dtc_cleared
            @messages_for_downstream << message_for_downstream(device_event_id, car_event_id, description, '00000')
          else
            dtc_list.each do |dtc_short_code|
              # find the trouble code object for this short code
              cdc = find_diagnostic_code_by_short_code(dtc_short_code)
              # if we don't know this trouble code, find the unknown trouble code object
              if cdc.blank?
                cdc = find_diagnostic_code_by_short_code(DataManager.opts[:diagnostic_trouble_codes][:unknown_dtc])

                # if we can't find the unknown trouble code object, we're screwed, so fail
                if cdc.blank?
                  fail DataManager::Errors::InvalidDiagnosticShortCode.new(dtc_short_code, @raw_message)
                else
                  device_event_id = DataManager.opts[:device_event_ids][:trouble_codes]
                  car_event_id = DataManager.opts[:car_event_ids][:trouble_code_found]
                  description = DataManager::ServiceUtils::Descriptions.vehicle_param_trouble_code_unknown(dtc_short_code)
                  DataManager.log description, lvl: :warn
                  @messages_for_downstream << message_for_downstream(device_event_id, car_event_id, description, dtc_short_code, cdc.Id)
                end
              else
                device_event_id = DataManager.opts[:device_event_ids][:trouble_code_event_message]
                car_event_id = DataManager.opts[:car_event_ids][:trouble_code_found]
                description = DataManager::ServiceUtils::Descriptions.vehicle_event_dtc_found
                @messages_for_downstream << message_for_downstream(device_event_id, car_event_id, description, dtc_short_code, cdc.Id)

                device_event_id = DataManager.opts[:device_event_ids][:trouble_codes]
                description = DataManager::ServiceUtils::Descriptions.vehicle_param_trouble_code(dtc_short_code, cdc.FriendlyDesc)
                @messages_for_downstream << message_for_downstream(device_event_id, car_event_id, description, dtc_short_code, cdc.Id)
              end
            end
          end
        end

      end
    end
  end
end
