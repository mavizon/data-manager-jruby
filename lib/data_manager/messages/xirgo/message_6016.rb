module DataManager
  module Messages
    module Xirgo
      class Message6016 < MajorityBase
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either
        # 6016: Idle Time threshold alert

        def initialize(raw_message, message_field_array)
          super(:'6016', raw_message, message_field_array)
        end

        private

        def persist_to_db_logic
          super do |message|
            #idle threshold exceeded event
            if message[:device_event_id] == DataManager.opts[:device_event_ids][:idle_event_message]
              update_car_idling(message)
              store_device_event_history(message, @raw_message)
              store_car_event_history(message)
              true
            else
              false
            end
          end
        end

        def messages_for_downstream
          super do
            #idle threshold exceeded event
            message = @base_message.dup
            message[:device_event_id] = DataManager.opts[:device_event_ids][:idle_event_message]
            message[:car_event_id] = DataManager.opts[:car_event_ids][:state_change]
            message[:description] = DataManager::ServiceUtils::Descriptions.vehicle_event_idle
            message[:idling_duration_secs] = DataManager.opts[:xirgo][:idle_timeout_secs]
            @messages_for_downstream << message
          end
        end

      end
    end
  end
end