module DataManager
  module Messages
    module Xirgo
      class Message6001 < MajorityBase
        #TODO: this is not fully implemented yet; I don't think it will fail, but it won't do anything special or message-specific, either
        # 6001: Direction change alert

        def initialize(raw_message, message_field_array)
          super(:'6001', raw_message, message_field_array)
        end

      end
    end
  end
end