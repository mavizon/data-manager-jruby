#require 'erb'
#require 'yaml'

module DataManager

  @config = Mavia::Config.instance
  @config.add_application_config do |config|
    #binding.pry
    config.load_config_in_dir_of_file(__FILE__)
  end
  #binding.pry
  attr_reader :config
  module_function :config

  def self.opts
    @config
  end

end