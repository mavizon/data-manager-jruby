module DataManager
  module ServiceUtils
    module Parsers
      module Xirgo

        module_function

        def parse(message_string)
          if !message_string.blank? && message_string.is_a?(String)
            put_in_final_form(split_full_message_at_delimiters(message_string))
          else
            fail DataManager::Errors::XirgoParseError.new("Message is either blank, or not a string. raw message: #{message_string}")
          end
        end

        private

        module_function

        def split_full_message_at_delimiters(message)
          message.split('##$$')
        end

        def put_in_final_form(message_array)
          final_message_array = []

          message_array.map do |s|
            temp_s = s.gsub('$$', '').gsub('##', '')
            invalid?(temp_s) ? next : final_message_array << split_record_into_fields(temp_s)
          end

          final_message_array
        end

        def split_record_into_fields(message)
          message.split(',')
        end

        def invalid?(message)
          if message.include?('$') || message.include?('#')
            DataManager.log DataManager::Errors::XirgoParseError.new("Message contains invalid characters. message: #{message}")
            true
          else
            false
          end
        end

      end
    end
  end
end
