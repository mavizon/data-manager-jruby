module DataManager
  module ServiceUtils
    module Descriptions

      module_function

      def vehicle_param_position(lat, lng)
        vehicle_position('update', lat, lng)
      end

      def vehicle_param_gps_history(lat, lng)
        vehicle_position('log', lat, lng)
      end

      def vehicle_param_speed(speed)
        "Vehicle traveling at #{speed} mph."
      end

      def vehicle_param_heading(heading)
        "Vehicle heading #{heading}."
      end

      def vehicle_param_speed_heading(speed, heading)
        "Vehicle heading #{heading} at #{speed} mph."
      end

      def vehicle_param_trouble_code(code, description)
        "Trouble Code #{code} found! #{description}"
      end

      def vehicle_param_trouble_code_unknown(code)
        "Unknown Trouble Code #{code} found!"
      end

      def vehicle_event_ignition
        'Vehicle started.'
      end

      def vehicle_event_park
        'Vehicle parked.'
      end

      def vehicle_event_driving
        'Vehicle in motion.'
      end

      def vehicle_event_idle
        'Vehicle idle.'
      end

      def vehicle_event_cel(description)
        "Check Engine Light is ON. #{description}"
      end

      def vehicle_event_cel_off
        'Check Engine Light is OFF.'
      end

      def vehicle_event_dtc_found
        'Diagnostic Trouble Code found.'
      end

      def vehicle_event_dtc_cleared
        'Diagnostic Trouble Code cleared.'
      end

      def vehicle_info_unknown_vehicle(vin)
        "Received messages from an unknown vehicle. VIN: #{vin}"
      end

      def vehicle_info_vehicle_moved
        'Device was moved from another vehicle.'
      end

      def vehicle_info_vehicle_not_moved(vin)
        "Device was not moved to new vehicle with vin: #{}"
      end

      def vehicle_event_param_fuel(fuel_level)
        "Vehicle fuel level: #{fuel_level}%."
      end

      def vehicle_event_param_voltage(voltage)
        "Vehicle voltage: #{voltage}."
      end

      def system_trip_summary(values)
        miles = values[:miles]
        mpg = values[:trip_mpg]
        avg_mph = values[:avg_speed]
        max_mph = values[:max_speed]
        trip_time = values[:total_trip_time]
        idle_time = values[:total_idle_time]
        fuel_used = values[:estimated_fuel]

        trip_summary_message = ''
        trip_summary_message << 'Trip summary: ' if miles || mpg || avg_mph || max_mph
        trip_summary_message << "#{miles} miles, " if miles
        trip_summary_message << "#{mpg} mpg, " if mpg
        trip_summary_message << "#{avg_mph} mph avg, " if avg_mph
        trip_summary_message << "#{max_mph} mph max, " if max_mph
        last_comma = trip_summary_message.rindex(',')
        trip_summary_message[last_comma] = '.' if last_comma

        trip_summary_message << "Trip time: #{trip_time} minutes. " if trip_time
        trip_summary_message << "Idle time: #{idle_time} minutes. " if idle_time
        trip_summary_message << "Fuel used: #{fuel_used} gallons." if fuel_used

        trip_summary_message.rstrip
      end

      private

      def self.vehicle_position(type, lat, lng)
        "GPS #{type}: #{lat} lat, #{lng} long."
      end

    end
  end
end