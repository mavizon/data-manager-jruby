#require 'yajl'
require 'json'

module DataManager
  module ServiceUtils
    module Encoders
      module JSON

        def encode(object)
          if object.is_a?(Hash) || object.is_a?(Array)
            ::JSON.generate(object)
          else
            fail DataManager::Errors::JSONEncodeError.new(object)
          end
        end

        module_function :encode

      end
    end
  end
end