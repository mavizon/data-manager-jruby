require 'base64'

module DataManager
  module ServiceUtils
    module Encoders
      module Base64

        def encode(body)
          ::Base64.strict_encode64(body)
        end

        module_function :encode
      end
    end
  end
end
