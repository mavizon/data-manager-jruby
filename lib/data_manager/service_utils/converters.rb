module DataManager
  module ServiceUtils
    module Converters

      module_function

      def kph_to_mph(kph)
        0.621371 * kph
      end

      def seconds_to_minutes(secs)
        secs / 60
      end
    end
  end
end