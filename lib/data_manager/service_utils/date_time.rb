module DataManager
  module ServiceUtils
    module DateTime

      # this is the number of seconds we subtract from an end-time to attempt to correct for messages with rep_times of slightly after the known 'end time' for a trip
      #TODO: move this to config
      END_TIME_SETBACK_SECONDS = 30

      module_function

      def unix_epoch
        @unix_epoch ||= Time.at(0).utc
      end

      # 4611599999 is seconds since epoch at 2116-02-19T23:59:59Z. Why 2116-02-19T23:59:59Z? Well, let's go to the MRI 2.0.0 docs:
      # Since Ruby 1.9.2, Time implementation uses a signed 63 bit integer, Bignum or Rational.
      # The integer is a number of nanoseconds since the Epoch which can represent 1823-11-12 to 2116-02-20. When Bignum or
      # Rational is used (before 1823, after 2116, under nanosecond), Time works slower as when integer is used.
      def end_of_days
        @end_of_days ||= Time.at(4611599999).utc
      end

      def yymmdd(date)
        if date.is_a?(Time)
          date.strftime('%y%m%d')
        else
          fail DataManager::Errors::InvalidClass.new("#{self.class}#yymmdd", Time, date.class)
        end
      end

      def hhmmss(date)
        if date.is_a?(Time)
          date.strftime('%H%M%S')
        else
          fail DataManager::Errors::InvalidClass.new("#{self.class}#hhmmss", Time, date.class)
        end
      end

      def end_time_with_setback(time)
        time - END_TIME_SETBACK_SECONDS
      end

      def now
        Time.now.utc
      end

    end
  end
end