module DataManager
  module ServiceUtils
    module GPS
      module DegreeAndDecimalMinutes

        # expects lat_degrees_minutes as float in the form DDMM.mmmmm where DD = degrees, MM = minutes and mmmmm = decimal minutes
        # Decimal degrees = whole number of degrees, plus minutes divided by 60. In order to handle negative values correctly
        # all calculations are done on non-negative values, and the final value is negated, if necessary, to yield the final
        # lat/lng
        def to_double(degrees_minutes, round_to = 6)
          neg = degrees_minutes < 0 ? true : false
          degrees_minutes = negate(degrees_minutes) if neg
          degrees = (degrees_minutes / 100).to_i
          minutes = degrees_minutes % 100
          loc = (degrees + (minutes / 60)).round(round_to)
          loc = negate(loc) if neg
          loc
        end

        module_function :to_double

        private

        def self.negate(number)
          -1 * number
        end

      end
    end
  end
end