require_relative 'inflections'

# holds 'inflector methods' (to use a Rails term) to change 'words' from one form to another
module DataManager
  module Inflector

    # dehumanize and to_sym
    def dehumanized_sym(word)
      word.dehumanize.to_sym
    end

    # underscore a word and then sub _ for ' ', ex: 'big Daddy' => 'big_daddy'
    def dehumanize(word)
      # should be OK to remove the respond_to? as long as we ensure we call this only on Strings or things that can be
      # cast to Strings
      #if word.respond_to?(:to_s)
      dh_word = word.underscore
      dh_word.gsub!(/ /, '_')
      dh_word
      #end
    end

    def underscore(camel_cased_word)
      # should be OK to remove the respond_to? as long as we ensure we call this only on Strings or things that can be
      # cast to Strings
      #if camel_cased_word.respond_to?(:to_s)
      word = camel_cased_word.to_s.dup
      word.gsub!(/::/, '/')
      word.gsub!(/(?:([A-Za-z\d])|^)(#{inflections.acronym_regex})(?=\b|[^a-z])/) { "#{$1}#{$1 && '_'}#{$2.downcase}" }
      word.gsub!(/([A-Z\d]+)([A-Z][a-z])/,'\1_\2')
      word.gsub!(/([a-z\d])([A-Z])/,'\1_\2')
      word.tr!("-", "_")
      word.downcase!
      word
      #end
    end

    module_function :dehumanize, :underscore, :dehumanized_sym
  end
end