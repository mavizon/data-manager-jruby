# encoding: utf-8
require_relative 'string_encoding'
require_relative 'inflector_methods'

# so there are some things from Rails I love, and have a hard time living without when using PORO.
# i'm adding them back here. thanks, Rails Core team ;)
class Hash
  def deep_symbolize_keys
    result = {}
    each do |key, value|
      result[(key.underscore.dehumanize.to_sym rescue key)] = value.is_a?(Hash) ? value.deep_symbolize_keys : value
    end
    result
  end

  alias_method :symbolize_keys, :deep_symbolize_keys
end

class Object
  # An object is blank if it's false, empty, or a whitespace string.
  # For example, "", "   ", +nil+, [], and {} are all blank.
  #
  # This simplifies:
  #
  #   if address.nil? || address.empty?
  #
  # ...to:
  #
  #   if address.blank?
  def blank?
    respond_to?(:empty?) ? empty? : !self
  end

  # An object is present if it's not <tt>blank?</tt>.
  def present?
    !blank?
  end

  # Returns object if it's <tt>present?</tt> otherwise returns +nil+.
  # <tt>object.presence</tt> is equivalent to <tt>object.present? ? object : nil</tt>.
  #
  # This is handy for any representation of objects where blank is the same
  # as not present at all. For example, this simplifies a common check for
  # HTTP POST/query parameters:
  #
  #   state   = params[:state]   if params[:state].present?
  #   country = params[:country] if params[:country].present?
  #   region  = state || country || 'US'
  #
  # ...becomes:
  #
  #   region = params[:state].presence || params[:country].presence || 'US'
  def presence
    self if present?
  end

  # Returns just the class name as a String, without namespace/modules
  # For example, this method called on class This::That::AndTheOtherThing will
  # return AndTheOtherThing
  def unqualified_class
    self.class.to_s.split('::').last
  end
end

class NilClass
  # +nil+ is blank:
  #
  #   nil.blank? # => true
  #
  def blank?
    true
  end
end

class FalseClass
  # +false+ is blank:
  #
  #   false.blank? # => true
  #
  def blank?
    true
  end
end

class TrueClass
  # +true+ is not blank:
  #
  #   true.blank? # => false
  #
  def blank?
    false
  end
end

class Array
  # An array is blank if it's empty:
  #
  #   [].blank?      # => true
  #   [1,2,3].blank? # => false
  #
  alias_method :blank?, :empty?
end

class Hash
  # A hash is blank if it's empty:
  #
  #   {}.blank?                # => true
  #   {:key => 'value'}.blank? # => false
  #
  alias_method :blank?, :empty?
end

class String
  # 0x3000: fullwidth whitespace
  NON_WHITESPACE_REGEXP = %r![^\s#{[0x3000].pack("U")}]! unless String.const_defined?(:NON_WHITESPACE_REGEXP)

  # A string is blank if it's empty or contains whitespaces only:
  #
  #   "".blank?                 # => true
  #   "   ".blank?              # => true
  #   "　".blank?               # => true
  #   " something here ".blank? # => false
  #
  def blank?
    # 1.8 does not takes [:space:] properly
    if encoding_aware?
      self !~ /[^[:space:]]/
    else
      self !~ NON_WHITESPACE_REGEXP
    end
  end

  def dehumanize
    DataManager::Inflector.dehumanize(self)
  end

  def underscore
    DataManager::Inflector.underscore(self)
  end

  def dehumanized_sym
    DataManager::Inflector.dehumanized_sym(self)
  end
end

class Numeric #:nodoc:
  # No number is blank:
  #
  #   1.blank? # => false
  #   0.blank? # => false
  #
  def blank?
    false
  end
end

class Time
  def to_s
    self.iso8601
  end
end
