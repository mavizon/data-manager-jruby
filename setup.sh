#!/bin/bash

cd .
rvmsudo bundle install --without development test
rvmsudo bundle update
rvmsudo foreman export -a data-manager-jruby -u ubuntu upstart /etc/init/