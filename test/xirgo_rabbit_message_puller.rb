#!/usr/bin/env ruby
lib = File.expand_path('../../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
env = ENV['MAVIA_ENV'] ? ENV['MAVIA_ENV'].to_sym : :development
require 'rubygems'
require 'bundler'
Bundler.require(:default, env)
require 'data_manager'

log_loc = File.expand_path('../xirgo_test_output8.txt', __FILE__)
message_log = File.new(log_loc, 'a')

begin
  Mavia::MessageQueues::Rabbit::Subscriber.new(DataManager.opts[:xirgo][:rabbit_url]).subscribe('xirgo.incoming.test', false, false, block: true) do |message|
    puts message
    message_log.puts message
  end
rescue => e
  puts e
ensure
  binding.pry
  message_log.close
end
