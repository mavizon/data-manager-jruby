def test_retry(tot, &block)
  total_retries = num_retries = tot
  total_retries.times do
    delay = block.call(total_retries, num_retries)
    num_retries -= 1
    puts delay
  end
end

test_retry(10) do |total_retries, num_retries|
  [Random.rand(1.0..2.0) * 0.1 * 2**(total_retries - num_retries), 2.0].min
end