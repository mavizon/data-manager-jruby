#!/usr/bin/env ruby
lib = File.expand_path('../../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
env = ENV['MAVIA_ENV'] ? ENV['MAVIA_ENV'].to_sym : :development
require 'rubygems'
require 'bundler'
Bundler.require(:default, env)
require 'data_manager'

log_loc = File.expand_path('../xirgo_test_output9.txt', __FILE__)

begin
  pub = Mavia::MessageQueues::Rabbit::Publisher.new(DataManager.opts[:xirgo][:rabbit_url])
  pub.create_exchange('xirgo')
  File.open(log_loc, 'r').each_line do |line|
    pub.publish('xirgo.incoming', line)
  end
rescue => e
  puts e
end
