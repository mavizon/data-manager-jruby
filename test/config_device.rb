"+XT:1008,2155863868"
"+XT:1001,41234,54.235.98.168,1"


#3050 --> 3040
#keys = "<ONI>,<ONA>,<OFI>,<OFA>,<DCT>,<SPT>,<RPT>,<MT>,<AC>,<DC>,<BT>,<PS>,<PI>,<PA>,<IDT>,<TW>,<TSTS>,<TSPS>,<TSTT>,<TSPT>,<MS>,<MSTS>,<MSPS>,<MSTT>,<MSPT>,<PT>,<PF>".gsub('<','').gsub('>','').split(',')
#values = "0.5,1,60,1,0,0,0,1000,0,0,8.0,2,10,1,2,1,20,5,10,120,0,20,5,10,120,0,MAVIA".split(',')
#config_3050 = Hash[keys.zip(values)]
config_3050 = {
     "ONI" => "0.5",
     "ONA" => "1",
     "OFI" => "240",
     "OFA" => "1",
     "DCT" => "0",
     "SPT" => "0",
     "RPT" => "0",
      "MT" => "1000",
      "AC" => "0",
      "DC" => "0",
      "BT" => "12.2",
      "PS" => "2",
      "PI" => "240",
      "PA" => "1",
     "IDT" => "2",
      "TW" => "1",
    "TSTS" => "20",
    "TSPS" => "5",
    "TSTT" => "10",
    "TSPT" => "120",
      "MS" => "0",
    "MSTS" => "20",
    "MSPS" => "5",
    "MSTT" => "10",
    "MSPT" => "120",
      "PT" => "0",
      "PF" => "MAVIA"
}
string_for_3040_text = "+XT:3040,#{config_3050.values.join(',')}"

"+XT:3040,0.5,1,240,1,0,0,0,1000,0,0,12.2,2,240,1,2,1,20,5,10,120,0,20,5,10,120,0,MAVIA"

#3052 --> 3042
#keys = "<SBHyst>,<SB1Begin>,<SB1End>,<SB2Begin>,<SB2End>,<SB3Begin>,<SB3End>,<SB4Begin>,<SB4End>,<SB5Begin>,<SB5End>,<DS>,<DRM>,<FT>,<MT>,<EC>,<MC>,<PF>".gsub('<','').gsub('>','').split(',')
#values = "0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1.00,1.00,MAVIA".split(',')
#config_3052 = Hash[keys.zip(values)]
config_3052 = {
      "SBHyst" => "0",
    "SB1Begin" => "0",
      "SB1End" => "0",
    "SB2Begin" => "0",
      "SB2End" => "0",
    "SB3Begin" => "0",
      "SB3End" => "0",
    "SB4Begin" => "0",
      "SB4End" => "0",
    "SB5Begin" => "0",
      "SB5End" => "0",
          "DS" => "1",
         "DRM" => "0",
          "FT" => "0",
          "MT" => "0",
          "EC" => "1.00",
          "MC" => "1.00",
          "PF" => "MAVIA"
}
string_for_3042_text = "+XT:3042,#{config_3052.values.join(',')}"

"+XT:3042,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1.00,1.00,MAVIA"
